     _______     __
    |_     _|.--|  |.-----.-----.-----.-----.-----.
     _|   |_ |  _  ||  _  |__ --|  -__|     |__ --|
    |_______||_____||_____|_____|_____|__|__|_____|
     http://www.idosens.com

Documentation protocole Idosens Solo
=====================================

_© Idosens 2019, ce document sous licence CC0 peut être diffusé librement._  
https://creativecommons.org/publicdomain/zero/1.0/deed.fr

1. Généralités
---------------

Idosens Solo est un produit grand public qui a été commercialisé par la société 
Idosens de 2016 à 2018. Le produit est composé d'un capteur, ses accessoires, 
associé à un service de connectivité et de présentation des données.

Le produit utilise le protocole de communication LoRaWAN v1.0, dans la bande 
868 MHz.

Deux révisions du produit ont été commercialisées :

__Versions 1__  
Identification de modèle : IDOSCA ou IDO1503-LPWAN  
EAN : 3701012700021  
AppEUI LoRaWAN : 0x70B3D54075CA1E00  
DevEUI LoRaWAN : de 0x70B3D54075CA1E00 à 0x70B3D54075CA1EBE

__Versions 2__, avec hygromètre  
Identification de modèle : IDOSCB  
EAN : 3701012700038  
AppEUI LoRaWAN : 0x70B3D54075CA1C00  
DevEUI LoRaWAN : de 0x70B3D54075CA1C01 à 0x70B3D54075CA1DFF

Sauf mention contraire, le reste du document ne s'applique qu'à la version 2 du 
capteur.

2. Sur-chiffrement
-------------------

En plus du chiffrement et de la signature cryptographique intégrée à LoRaWAN, 
le contenu du message envoyé par le capteur Solo est protégé par un masque de 
confidentialité afin d'assurer un niveau supplémentaire de sécurité lors du 
transit à travers le réseau de l'opérateur LoRaWAN.

L'algorithme de chiffrement XTEA doit être compatible avec la version contenue 
dans la librairie Mbed-crypt, c'est à dire donner un résultat identique aux 
vecteurs de test qu'on trouve dans les sources.
https://github.com/ARMmbed/mbed-crypto/blob/development/library/xtea.c
Seule la fonction 'ENCRYPT' est utilisée, jamais la fonction 'DECRYPT'.

A partir d'un nonce de 64 bits, la fonction de chiffrement XTEA est utilisée 
avec la clé principale LoRaWAN (AppKey, 128 bits) pour générer un masque de 
confidentialité de 64 bits. Le nonce est composé ainsi :

 - octet 0 (MSB) = constante 0x1D
 - octet 1 = constante 0x50
 - octet 2 = constante 0x02
 - octet 3 = octet 7 (LSB) du DevEUI LoRaWAN
 - octet 4 = octet 6 du DevEUI LoRaWAN
 - octet 5 = octet 5 du DevEUI LoRaWAN
 - octet 6 = MSB du compteur de paquet LoRaWAN
 - octet 7 (LSB) = LSB du compteur de paquet LoRaWAN

La génération du masque de confidentialité se fait exactement de la même façon 
du côté capteur (émission du message) et du côté du système qui va interpréter 
les données envoyée par le capteur (réception du message). Le masque de 
confidentialité est appliqué sur le message avec une fonction XOR (ou-exclusif).
En pratique, puisque le message fait trois octets, seuls les trois premiers 
octets (octets de poids fort) du masque sont utilisés.

__Emission :__ message en clair XOR masque => message protégé  
__Réception :__ message protégé XOR masque => message en clair


3. Décodage
------------

Les données envoyées par le capteur (message) sont composées de 3 octets.

      octet 0           octet 1           octet 2           
     +-----------------+-----------------+-----------------+
     | bit 7 ... bit 0 | bit 7 ... bit 0 | bit 7 ... bit 0 |
     +-----------------+-----------------+-----------------+

Après retrait du masque de confidentialité, les données s'interprètent ainsi :

 * Octet 0, bit 7 (bit de poids fort) : état de la porte
     - 0 = la porte est fermée
     - 1 = la porte est ouverte

 * Octet 0, bit 6 : signal d'évènement
     - 0 = message envoyé périodiquement
     - 1 = message envoyé immédiatement suite à l'ouverture de la porte

 * Octet 0, bits 5 à 4 : indication d'état des piles
     - 00 = piles usées
     - ...
     - 11 = piles ok

 * Octet 0, bits 3 à 0 : mesure de luminosité sur une échelle de 300 lux
     - 0000 = erreur capteur
     - 0001 = luminosité 0 à 10%
     - 0010 = luminosité 10 à 20%
     - ...
     - 1001 = luminosité 80 à 90%
     - 1010 = luminosité 90 à 100%
     - 1011 = luminosité supérieure à 100% (>300 lux)
     - 1100 à 1100 = valeurs invalides

 * Octet 1 : entier 8 bit signé, température en demi-degrés Celsius
     - 0x3F = +63,5 °C
     - ...
     - 0x01 = + 0,5 °C
     - 0x00 =   0   °C
     - 0xFF = - 0,5 °C
     - ...
     - 0x80 = -64,0 °C (capteur utilisable jusqu'à -40 °C)

 * Octet 2, bit 7 : passe à 1 lorsque le message a été répété

 * Octet 2, bits 6 à 0 : entier 7 bit non signé, humidité relative en %
     - 0x00 = 0% d'humidité relative
     - ...
     - 0x64 = 100% d'humidité relative
     - 0x65 à 0x8F = valeurs invalides

4. Justesse des mesures
------------------------

L'humidité et la température sont mesurées par un capteur Si7006-A20 de chez 
Silicon Labs.

La luminosité est mesurée par un capteur TSL2571 de chez AMS, avec une 
ouverture limitée dans le boitier.

5. Exemple
-----------

Les exemples de paquet qui suivent sont issus d'un capteur avec les 
caractéristiques suivantes :

 - type Solo v2
 - Dev EUI = 0x70B3D54075CA1D7D
 - App Key = 0xD0B97194C9B3B1FC1FBFFC16DD0DC76F

Les données qui suivent sont représentées sous forme de tableau d'octets, en 
notation hexadécimale, dans l'ordre "naturel" avec les octets, du plus fort au 
plus faible, dans le sens de lecture de gauche à droite.

### 5.1. Paquet n° 1

Contenu du paquet LoRaWAN = [6D AE B5]

Nonce = [1D 50 02 7D 1D CA 00 01]  
Résultat XTEA = [5E 82 F4 E1 E6 EA 95 DB]  
Masque de confidentialité = [5E 82 F4] (trois MSB du résultat XTEA)  
Contenu sans sur-chiffrement = [33 2C 41]

Interprétation  
 -> porte fermée  
 -> piles ok  
 -> luminosité 20 à 30%  
 -> température 22,0 °C  
 -> paquet non répété  
 -> humidité relative 65%

### 5.2. Paquet n° 2

Contenu du paquet LoRaWAN = [4D E1 3A]

Nonce = [1D 50 02 7D 1D CA 00 02]  
Résultat XTEA = [BF CD 7B F7 50 8F F7 B8]  
Contenu sans sur-chiffrement = [F2 2C 41]

Interprétation  
 -> porte ouverte, évènement  
 -> piles ok  
 -> luminosité 10 à 20%  
 -> température 22,0 °C  
 -> paquet non répété  
 -> humidité relative 65%

### 5.3. Paquet n° 3

Contenu du paquet LoRaWAN = [9D AB 01]

Nonce = [1D 50 02 7D 1D CA 00 03]  
Résultat XTEA = [6F 87 C0 AC A9 75 53 5D]  
Contenu sans sur-chiffrement = [F2 2C C1]

Interprétation  
 -> porte ouverte, évènement  
 -> piles ok  
 -> luminosité 10 à 20%  
 -> température 22,0 °C  
 -> paquet répété  
 -> humidité relative 65%

### 5.4. Paquet n° 50

Contenu du paquet LoRaWAN = [4F C3 90]

Nonce = [1D 50 02 7D 1D CA 00 32]  
Résultat XTEA = [FE EB AE 48 A4 EE A5 E9]  
Contenu sans sur-chiffrement = [B1 28 3E]

Interprétation  
 -> porte ouverte  
 -> piles ok  
 -> luminosité 0 à 10%  
 -> température 20,0 °C  
 -> paquet non répété  
 -> humidité relative 62%
