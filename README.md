Protocole Idosens Solo
=======================

Ce dépôt de fichiers contient la documentation du protocole utilisé par le 
produit Solo de la marque Idosens pour encoder les données transportées via une 
liaison LoRaWAN.

protocole.pdf : documentation du protocole au format PDF

protocole.txt : documentation du protocole en texte "brut", avec formattage Markdown.